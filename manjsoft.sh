#!/bin/bash
#set -e
##################################################################################################################
# original Author 	: Erik Dubois
# Website	: https://www.arcolinux.info
# Author : Steve
##################################################################################################################
#
#   DO NOT JUST RUN THIS. EXAMINE AND JUDGE. RUN AT YOUR OWN RISK.
#
##################################################################################################################

#software from Manjaro Linux repositories put # in front of software you dont want.

#sudo pacman -S --noconfirm --needed thunderbird
sudo pacman -S --noconfirm --needed audacity
sudo pacman -S --noconfirm --needed dropbox
sudo pacman -S --noconfirm --needed picard
sudo pacman -S --noconfirm --needed bleachbit
sudo pacman -S --noconfirm --needed keepassxc
sudo pacman -S --noconfirm --needed obs-studio
sudo pacman -S --noconfirm --needed transmission-gtk
sudo pacman -S --noconfirm --needed soundconverter
sudo pacman -S --noconfirm --needed shotcut
sudo pacman -S --noconfirm --needed sublime-text
#sudo pacman -S --noconfirm --needed mugshot
sudo pacman -S --noconfirm --needed bookworm
sudo pacman -S --noconfirm --needed libreoffice-fresh-en-gb
#sudo pacman -S --noconfirm --needed telegram-desktop
#sudo pacman -S --noconfirm --needed discord
sudo pacman -S --noconfirm --needed syncthing-gtk
sudo pacman -S --noconfirm --needed gocryptfs
sudo pacman -S --noconfirm --needed simplescreenrecorder
#sudo pacman -S --noconfirm --needed

###############################################################################################

echo "################################################################"
echo "################### core software installed"
echo "################################################################"
